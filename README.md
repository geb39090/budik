# Budik

Závěrečný projekt
Budík řízený STM8 Nucleo, 24hod formát (00:00-23:59)
Minuty běží jako vteřiny
Na vstupu jsou 2 tlačítka a na výstupu 4 7mi segmenty

# Blokové schéma
```mermaid
flowchart LR
    PC-->STM8
    BTN_1-->STM8
    BTN_2-->STM8
subgraph STM8 Nucleo
    STM8-->G2-7
    STM8-->C4-7,D3-4
    STM8-->B0-5,D2
    STM8-->A6-3,E6-7
    end
subgraph Segmenty
    G2-7-->1.
    C4-7,D3-4-->2.
    B0-5,D2-->3.
    A6-3,E6-7-->4.
    end   
```    
# Seznam součástek
| Typ součastky | Hodnota | Počet kusů |  Cena za kus |  Cena dohromady |
|:-------------:|:-------:|:----------:|:------------:|:---------------:|
|   Tlačítka    |         |      2     |      5Kč     |       10Kč      |
| 7seg. displej |         |      4     |      7Kč     |       28Kč      |
|    Rezistor   |   330R  |     28     |      2Kč     |       56Kč      |
|     Drátky    |         |     30     |      1Kč     |       30Kč      |
| STM8 Nucleo   |         |      1     |    250Kč     |      250Kč      |
| Celkem:       |         |            |              |      374Kč      |

# Zapojení
![Zapojení na nepájívém poli](budik-zapojeni.jpg)

# Kicad schéma
![Schémá zapojení v KiCadu](budik-schema2.png)

# Zdrojový kód
Zdrojový kód je součástí přílohy budik-kod v souboru .zip a nebo na 
master branchi

# Závěr
Jsem zklamaný že jsem projekt nestihl dotáhnout úplně do konce, ale
svůj úkol dá se říci že splňuje    

# Autor
  Dominik Gebel



